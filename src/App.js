// import logo from './logo.svg';
import React, { useEffect, useState } from 'react';
import {  Col, InputNumber, Layout, Row,Input } from 'antd';
import { Select } from 'antd';
import './App.less';


function App() {
  const { Option } = Select;
  const [valueInpurNuber, setValueInputNumber] = useState();
  const [isPrime, setIsPrime] = useState(false);

  useEffect(() => {
    const valueName =  Number.isInteger(valueInpurNuber)
      if (!valueName) {
         setValueInputNumber(Math.round(valueInpurNuber));
      } else if (valueInpurNuber < 0) {
         setValueInputNumber(1);
      }
  },[valueInpurNuber])

  // const checkValue = (e) => {
  //   console.log(e);
  //   const valueName =  Number.isInteger(e)
  //   if (!valueName) {
  //      setValueInputNumber(Math.round(e));
  //   } else if (e < 0) {
  //      setValueInputNumber(1);
  //   }
  // }

  const checkPrime = (num) => {
    if(num < 2) return false;

    for (let k = 2; k < num; k++){
      if( num % k === 0){
        return false;
      }
    }
    return true;
  }
 
  const calculateNumber = (value) => {
    if (value === "isPrime") {
      if (checkPrime(valueInpurNuber)) {
          setIsPrime(true)
      } else {
        setIsPrime(false)
      }
    }
  }
  
  return (
    <Layout className="App">
      <Row className="main-layout">
        <Col span={4} className="box1">
          <InputNumber
            onChange={setValueInputNumber}
            value={valueInpurNuber}
          />
        </Col>
        <Col span={14} className="box2">
          <Select style={{ width: 200 }} onChange={calculateNumber}>
            <Option value="isPrime">isPrime</Option>
            <Option value="isFibonacci">isFibonacci</Option>

          </Select>
        </Col>
        <Col span={6} className="box3">
         <Input defaultValue={isPrime}/>
        </Col>
      </Row>

    </Layout>
 
  );
}

export default App;
